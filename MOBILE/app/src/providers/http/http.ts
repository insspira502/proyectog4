import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';

/*
  Generated class for the HttpProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class HttpProvider {

  aplicacion : string = 'http://104.198.216.252/';
  
  constructor(public http: HttpClient) {
    console.log('Hello HttpProvider Provider');

  }

  metodo_usuario_crear(nombre:String,apellido:String,contrasena:String){
    let path_aux  = this.aplicacion + 'crear_usuario?nombre=' + nombre + '&apellido=' + apellido + '&clave=' + contrasena;
    console.log(path_aux);
    return this.http
    .get(path_aux)
    .toPromise();
  }

  metodo_login(nombre:String,contrasena:String){
    let path_aux  = this.aplicacion + 'login?nombre=' + nombre + '&clave=' + contrasena;
    console.log(path_aux);
    return this.http
    .get(path_aux)
    .toPromise();
  }   

  metodo_negocio_crear(nombre:String,propietario:String,ubicacion:String,descripcion:String){
    let path_aux  = this.aplicacion + 'negocio_crear?nombre=' + nombre + '&propietario=' + propietario + '&ubicacion=' + ubicacion+ '&descripcion=' + descripcion;
    console.log(path_aux);
    return this.http
    .get(path_aux)
    .toPromise();
  }  

  metodo_negocio_show(propietario:String){
    let path_aux  = this.aplicacion + 'negocio_show?propietario=' + propietario;
    console.log(path_aux);    
    return this.http
    .get(path_aux)
    .toPromise();
  }  

  metodo_agregar_producto(nombre:String,codigo:String,lote:String,fecha:String,descripcion:String,precio:String){
    let path_aux  = this.aplicacion + 'agregar_producto?nombre=' + nombre + '&codigo=' + codigo + '&lote=' + lote + '&fecha=' + fecha + '&descripcion=' + descripcion + '&precio=' + precio;
    console.log(path_aux);
    return this.http
    .get(path_aux)
    .toPromise();
  }

  metodo_productos_show(){
    let path_aux  = this.aplicacion + 'productos_show';
    console.log(path_aux);    
    return this.http
    .get(path_aux)
    .toPromise();
  }    

  metodo_contabilizar( nombre_negocio ,producto,cantidad,fecha){
    let path_aux  = this.aplicacion + 'contabilizar?negocio=' + nombre_negocio + '&producto=' + producto + '&cantidad='+cantidad + '&fecha='+fecha;
    console.log(path_aux);
    return this.http
    .get(path_aux)
    .toPromise();    
  }

  listar_stock_actual(negocio){
    let path_aux  = this.aplicacion + 'stock_actual?negocio=' + negocio;
    console.log(path_aux);    
    return this.http
    .get(path_aux)
    .toPromise();
  }    

  listar_stock_recomendaciones(){
    let path_aux  = this.aplicacion + 'recomendaciones';
    console.log(path_aux);    
    return this.http
    .get(path_aux)
    .toPromise();
  }      


}
